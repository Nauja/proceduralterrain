package com.proceduralterrain.generator;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.imageio.ImageIO;

public abstract class PNGGenerator extends CommandGenerator {

	@Override
	public Map<String, Object> execute(Map<String, Object> params)
			throws InterruptedException, IOException {
		Map<String, Object> results = super.execute(params);
		getResults(results);
		return results;
	}

	public abstract void getResults(Map<String, Object> results);

	public static Object loadImage(File file) {
		try {
			return ImageIO.read(file);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

}
