package com.proceduralterrain.generator;

import java.util.Map;

public interface IGenerator {
	
	public Map<String, Object> execute(Map<String, Object> params) throws Exception;

}
