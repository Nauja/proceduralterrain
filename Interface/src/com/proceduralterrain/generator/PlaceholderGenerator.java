package com.proceduralterrain.generator;

import java.util.HashMap;
import java.util.Map;

public abstract class PlaceholderGenerator implements IGenerator {

	@Override
	public Map<String, Object> execute(Map<String, Object> params)
			throws Exception {
		Map<String, Object> results = new HashMap<String, Object>();
		getResults(results);
		return results;
	}

	public abstract void getResults(Map<String, Object> results);

}
