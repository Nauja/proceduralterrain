package com.proceduralterrain.generator;

import java.io.File;
import java.util.Map;

public class HeightMapGenerator extends PNGGenerator {

	public static final IGenerator PLACEHOLDER = new PlaceholderGenerator() {

		@Override
		public void getResults(Map<String, Object> results) {
			results.put(HEIGHTMAP, loadImage(new File("../Biome/heightmap.png")));
		}
	};
	
	public static final String HEIGHTMAP = "heightMap";

	@Override
	public void getResults(Map<String, Object> results) {
	}

	@Override
	public String getCommandName() {
		return null;
	}

	@Override
	public File getCommandPath() {
		return null;
	}

}
