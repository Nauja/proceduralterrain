package com.proceduralterrain.generator;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public abstract class CommandGenerator implements IGenerator {

	@Override
	public Map<String, Object> execute(Map<String, Object> params) throws InterruptedException, IOException {
		ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/C", getCommandName());
		pb.directory(getCommandPath());

		Process p = pb.start();
		AfficheurFlux fluxSortie = new AfficheurFlux(p.getInputStream());
		AfficheurFlux fluxErreur = new AfficheurFlux(p.getErrorStream());
		new Thread(fluxSortie).start();
		new Thread(fluxErreur).start();
		p.waitFor();
		
		return new HashMap<String, Object>();
	}

	public abstract String getCommandName();
	
	public abstract File getCommandPath();

}
