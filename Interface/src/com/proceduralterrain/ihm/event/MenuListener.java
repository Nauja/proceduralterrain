package com.proceduralterrain.ihm.event;

import java.util.EventListener;

/**
 * Menu listener.
 */
public interface MenuListener extends EventListener {
	
	public void onFileExit();

}
