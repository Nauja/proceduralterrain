package com.proceduralterrain.ihm.options;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.event.EventListenerList;

import com.proceduralterrain.ihm.options.event.OptionsListener;

/**
 * Options panel.
 */
public class OptionsPane extends JPanel {

	/**
	 * Serial version UID.
	 */
	private static final long serialVersionUID = -4213768699999593000L;

	/**
	 * Listeners.
	 */
	private EventListenerList listeners;

	/**
	 * Global temperature.
	 */
	private JSpinner globalTemperature;

	/**
	 * Generate button.
	 */
	private JButton generate;

	/**
	 * Default constructor.
	 */
	public OptionsPane() {
		super();
		JPanel groups = createGroups();
		setLayout(new BorderLayout());
		add(groups, BorderLayout.NORTH);
	}

	private JPanel createGroups() {
		Box box = Box.createVerticalBox();
		// Terrain.
		{
			JPanel content = new JPanel();
			box.add(createGroup("Terrain", content));
		}
		box.add(Box.createRigidArea(new Dimension(0, 5)));
		// Temperature.
		{
			JPanel content = new JPanel();
			content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));
			content.setAlignmentX(JPanel.LEFT_ALIGNMENT);
			JLabel label = new JLabel("Globale:");
			label.setAlignmentX(JLabel.LEFT_ALIGNMENT);
			content.add(label);
			globalTemperature = new JSpinner();
			content.add(globalTemperature);
			box.add(createGroup("Température", content));
		}
		box.add(Box.createRigidArea(new Dimension(0, 5)));
		// Generate.
		{
			JPanel content = new JPanel();
			content.setLayout(new BoxLayout(content, BoxLayout.X_AXIS));
			generate = new JButton("Generate");
			content.add(generate);
			generate.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					fireOnGenerate();
				}
			});
			box.add(content);
		}
		return wrap(box);
	}

	private static JPanel createGroup(String title, JPanel content) {
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createTitledBorder(title));
		panel.setLayout(new BorderLayout());
		panel.add(wrap(content), BorderLayout.CENTER);
		return panel;
	}

	private static JPanel wrap(Component content) {
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.add(Box.createRigidArea(new Dimension(5, 0)), BorderLayout.WEST);
		panel.add(Box.createRigidArea(new Dimension(5, 0)), BorderLayout.EAST);
		panel.add(Box.createRigidArea(new Dimension(0, 5)), BorderLayout.NORTH);
		panel.add(Box.createRigidArea(new Dimension(0, 5)), BorderLayout.SOUTH);
		panel.add(content, BorderLayout.CENTER);
		return panel;
	}

	public void addListener(OptionsListener listener) {
		listeners.add(OptionsListener.class, listener);
	}

	public void removeListener(OptionsListener listener) {
		listeners.remove(OptionsListener.class, listener);
	}

	private void fireOnGenerate() {
		for (OptionsListener l : listeners.getListeners(OptionsListener.class)) {
			l.onGenerate();
		}
	}

}
