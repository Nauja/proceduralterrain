package com.proceduralterrain.ihm.options.event;

import java.util.EventListener;

public interface OptionsListener extends EventListener {
	
	public void onGenerate();

}
