package com.proceduralterrain.ihm.viewer;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 * Viewer pane.
 */
public class ViewerPane extends JTabbedPane {

	/**
	 * Serial version UID.
	 */
	private static final long serialVersionUID = -3919509792570062172L;
	
	/**
	 * Height map pane.
	 */
	private JPanel heightMapPane;
	
	/**
	 * Default constructor.
	 */
	public ViewerPane() {
		super();
		heightMapPane = createPNGPane();
		add("Hauteur", heightMapPane);
	}
	
	private static JPanel createPNGPane() {
		JPanel panel = new JPanel();
		return panel;
	}

}
