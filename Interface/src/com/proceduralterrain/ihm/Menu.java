package com.proceduralterrain.ihm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.event.EventListenerList;

import com.proceduralterrain.ihm.event.MenuListener;

/**
 * Menu.
 */
public class Menu extends JMenuBar {

	/**
	 * Serial version UID.
	 */
	private static final long serialVersionUID = 4763352652695749785L;
	
	/**
	 * Listeners.
	 */
	private EventListenerList listeners;
	
	/**
	 * Exit.
	 */
	private JMenuItem fileExit;

	/**
	 * Default constructor.
	 */
	public Menu() {
		super();
		fileExit = new JMenuItem("Exit");
		JMenu file = new JMenu("File");
		file.add(fileExit);
		add(file);
		// Events.
		listeners = new EventListenerList();
		fileExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				notifyFileExit();
			}
		});
	}
	
	/**
	 * Add given listener.
	 * @param listener listener to add.
	 */
	public void addListener(MenuListener listener) {
		listeners.add(MenuListener.class, listener);
	}
	
	private void notifyFileExit() {
		for(MenuListener l : listeners.getListeners(MenuListener.class)) {
			l.onFileExit();
		}
	}
	
}
