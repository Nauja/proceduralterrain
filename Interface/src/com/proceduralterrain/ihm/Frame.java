package com.proceduralterrain.ihm;

import javax.swing.JFrame;

import com.proceduralterrain.ihm.event.MenuListener;
import com.proceduralterrain.ihm.options.event.OptionsListener;

/**
 * Main frame.
 */
public class Frame extends JFrame implements MenuListener, OptionsListener {
	
	/**
	 * Serial version UID.
	 */
	private static final long serialVersionUID = 3374731506247398797L;
	
	/**
	 * Main panel.
	 */
	private Panel panel;
	
	/**
	 * Menu.
	 */
	private Menu menu;
	
	/**
	 * Default constructor.
	 */
	public Frame() {
		super();
		panel = new Panel();
		menu = new Menu();
		menu.addListener(this);
		setTitle("Procedural Terrain");
		setContentPane(panel);
		setJMenuBar(menu);
		pack();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onFileExit() {
		System.exit(0);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onGenerate() {
		
	}

}
