package com.proceduralterrain.ihm;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.JPanel;

import com.proceduralterrain.ihm.options.OptionsPane;
import com.proceduralterrain.ihm.viewer.ViewerPane;

/**
 * Main panel.
 */
public class Panel extends JPanel {

	/**
	 * Serial version UID.
	 */
	private static final long serialVersionUID = 953607247500469836L;

	/**
	 * Viewer pane.
	 */
	private ViewerPane viewerPane;

	/**
	 * Options pane.
	 */
	private OptionsPane optionsPane;

	/**
	 * Default constructor.
	 */
	public Panel() {
		super();
		setLayout(new BorderLayout());
		// Left tabbed pane.
		JPanel leftPane = new JPanel();
		leftPane.setLayout(new BorderLayout());
		viewerPane = new ViewerPane();
		leftPane.add(viewerPane, BorderLayout.CENTER);
		leftPane.add(Box.createRigidArea(new Dimension(256, 0)),
				BorderLayout.SOUTH);
		leftPane.add(Box.createRigidArea(new Dimension(0, 256)),
				BorderLayout.EAST);
		// Right options pane.
		JPanel rightPane = new JPanel();
		rightPane.setLayout(new BorderLayout());
		optionsPane = new OptionsPane();
		rightPane.add(optionsPane, BorderLayout.CENTER);
		rightPane.add(Box.createRigidArea(new Dimension(150, 0)),
				BorderLayout.SOUTH);
		// .
		add(leftPane, BorderLayout.CENTER);
		add(rightPane, BorderLayout.EAST);
	}

	public ViewerPane getViewerPane() {
		return viewerPane;
	}

	public OptionsPane getOptionsPane() {
		return optionsPane;
	}

}
