package com.proceduralterrain;

import java.io.IOException;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import com.proceduralterrain.ihm.Frame;

public class Main {

	/**
	 * Main.
	 * 
	 * @param args
	 *            arguments.
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws IOException,
			InterruptedException {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					new Frame();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
