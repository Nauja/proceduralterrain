# -*- coding: cp1252 -*-
from random import random
import base
from base import Cmd, DrawCmd, Renderer, RuleMgr, Rule
from PIL import Image
from math import exp, log

# Utils.
def apply(source, destination, f, param):
    destinationPix = destination.load()
    sourcePix = source.load()
    w = source.size[0]
    h = source.size[1]
    for i in xrange(w):
            for j in xrange(h):
                destinationPix[i, j] = f(w, h, i, j, sourcePix[i, j], param)
    return destination

# Commandes.
class HeightMap(base.Cmd):
        def __init__(self, heightMap, niveauEau, temperatureGlobale):
                base.Cmd.__init__(self, 'HeightMap', {'heightMap': heightMap, 'niveauEau': niveauEau, 'temperatureGlobale': temperatureGlobale})

class TemperatureMap(base.Cmd):
        def __init__(self, temperatureMap):
                base.Cmd.__init__(self, 'TemperatureMap', temperatureMap)

class PrecipitationMap(base.Cmd):
        def __init__(self, precipitationMap):
                base.Cmd.__init__(self, 'PrecipitationMap', precipitationMap)

# R�gles.
class GenerateRule(base.Rule):
        def match(self, cmd):
            return cmd.name == 'HeightMap'

        def apply(self, cmd):
            tm = self.temperatureMap(cmd)
            pm = self.precipitationMap(cmd)
            return [cmd, tm, pm];

        def temperatureMap(self, cmd):
            tm = apply(cmd.param['heightMap'], Image.new("L", cmd.param['heightMap'].size, "black"), self.temperatureMapPixel, cmd.param)
            return TemperatureMap(tm)

        def precipitationMap(self, cmd):
            pm = apply(cmd.param['heightMap'], Image.new("L", cmd.param['heightMap'].size, "black"), self.precipitationMapPixel, cmd.param)
            return PrecipitationMap(pm)

        def temperatureMapPixel(self, w, h, x, y, value, param):
            hauteur = max(0, (value - param['niveauEau']) / float(255 - param['niveauEau']))
            hTmp = (((hauteur * hauteur) * (255 - 100) + 100) + param['temperatureGlobale']) / 2.0
            return min(255, max(0, 255 - hTmp))

        def precipitationMapPixel(self, w, h, x, y, value, param):
            return value

# Dessins.
class DrawTemperatureMap(base.DrawCmd):
	def match(self, cmd):
		return cmd.name == 'TemperatureMap'
		
	def execute(self, cmd, context):
                cmd.param.save("temperaturemap.png")
                self.enhance(cmd.param).save("temperaturemap2.png")

        def enhance(self, temperatureMap):
                tm = Image.new("RGB", temperatureMap.size, "black")
                tmPix = tm.load()
                temperatureMapPix = temperatureMap.load()
                for i in xrange(temperatureMap.size[0]):
                        for j in xrange(temperatureMap.size[1]):
                                tmPix[i, j] = self.enhancePixel(temperatureMapPix[i, j])
                return tm

        def enhancePixel(self, value):
                if value <= 255 / 2:
                        g = value * 2
                else:
                        g = (255 - value) * 2
                return (value, g, 255 - value)
                
class DrawPrecipitationMap(base.DrawCmd):
	def match(self, cmd):
		return cmd.name == 'PrecipitationMap'
		
	def execute(self, cmd, context):
                cmd.param.save("precipitationmap.png")

# Renderer.
class ImageRenderer(base.Renderer):
        def __init__(self, drawing_cmds):
                base.Renderer.__init__(self, drawing_cmds)

        def post_draw(self, context):
                pass

        def get_context(self):
                return {}

def test(heightMap):
    # Cr�ation de la commande contenant la HeightMap.
    im = Image.open(heightMap).convert("L")
    niveauEau = 50
    temperatureGlobale = 100
    cmd = [HeightMap(im, niveauEau, temperatureGlobale)]
    #==
    rulemgr = RuleMgr( [ GenerateRule() ])
    cmds = rulemgr.apply(cmd, 1)
    #==
    drawing_cmds = [
                DrawTemperatureMap(),
                DrawPrecipitationMap()]
    renderer = ImageRenderer(drawing_cmds)
    renderer.draw(cmds)

if __name__ == "__main__":
    test("heightmap.png")
