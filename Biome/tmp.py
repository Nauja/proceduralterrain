import turtle
from random import random
import base
from base import Cmd, DrawCmd, Renderer, RuleMgr, Rule
import pyglet
from pyglet import gl 
from math import cos, sin, radians
from mesh import Pyramid, Shape, rotate, translate, Dodecahedron
from obj_export import write_file
from path import Path
from vector import Vector

# Utils.
def expand(distance, largeur):
	largeur2 = largeur * 0.75
	largeur3 = largeur2 * 0.5
	largeur4 = largeur2 * 0.5
	list = []
	list.append(Forward(distance, largeur, largeur2))
	list.append(Push())
	list.append(Rotate(-45))
	list.append(Forward(distance * 0.75, largeur3, largeur3 * 0.75))
	list.append(Pop())
	list.append(Push())
	list.append(Rotate(45))
	list.append(Forward(distance * 0.75, largeur3, largeur3 * 0.75))
	list.append(Pop())
	return list
	
class State(object):
	def __init__(self, position, angleX, angleY, angleZ):
		self.position = position
		self.angleX = angleX
		self.angleY = angleY
		self.angleZ = angleZ

# Commands.
class Forward(base.Cmd):
    def __init__(self, distance, tailleDebut, tailleFin):
        base.Cmd.__init__(self, 'forward', {
			'distance': distance,
			'tailleDebut': tailleDebut,
			'tailleFin': tailleFin
		})
		
class Rotate(base.Cmd):
    def __init__(self, angle):
        base.Cmd.__init__(self, 'rotate', {
			'angle': angle
		})

class Push(base.Cmd):
	def __init__(self):
		base.Cmd.__init__(self, 'push')

class Pop(base.Cmd):
	def __init__(self):
		base.Cmd.__init__(self, 'pop')
		
# Rules.
class SplitRule(base.Rule):
    def match(self, cmd):
        return cmd.name == 'forward'

    def apply(self, cmd):
        return expand(cmd.param['distance'], cmd.param['tailleFin']);

# Drawing commands.
class DrawForward(base.DrawCmd):
	def match(self, cmd):
		return cmd.name == 'forward'
		
	def execute(self, cmd, context):
		position = context['position']
		distance = cmd.param['distance']
		tailleDebut = cmd.param['tailleDebut']
		tailleFin = cmd.param['tailleFin']
		mesh = Pyramid(tailleDebut, distance/2.0, tailleDebut, tailleFin, distance/2.0, tailleFin)
		translate(mesh, Vector(0, distance/2.0, 0))
		rotate(mesh, Vector(1, 0, 0), context['angleX'])
		rotate(mesh, Vector(0, 1, 0), context['angleY'])
		rotate(mesh, Vector(0, 0, 1), context['angleZ'])
		translate(mesh, position)
		context['meshs'].append(mesh)
		p = Vector(0, distance, 0)
		p.rotate(Vector(1, 0, 0), context['angleX'])
		p.rotate(Vector(0, 1, 0), context['angleY'])
		p.rotate(Vector(0, 0, 1), context['angleZ'])
		context['position'] = p + position
		mesh = Dodecahedron(tailleFin)
		translate(mesh, context['position'])
		context['meshs'].append(mesh)
			
class DrawRotate(base.DrawCmd):
	def match(self, cmd):
		return cmd.name == 'rotate'
		
	def execute(self, cmd, context):
		context['angleZ'] += cmd.param['angle']
	
class DrawPush(base.DrawCmd):
	def match(self, cmd):
		return cmd.name == 'push'
		
	def execute(self, cmd, context):
		state = [State(context['position'], context['angleX'], context['angleY'], context['angleZ'])]
		if 'states' not in context:
			context['states'] = state
		else:
			context['states'] = state + context['states']
		
class DrawPop(base.DrawCmd):
	def match(self, cmd):
		return cmd.name == 'pop'
		
	def execute(self, cmd, context):
		state = context['states'][0]
		context['states'] = context['states'][1:]
		context['position'] = state.position
		context['angleX'] = state.angleX
		context['angleY'] = state.angleY
		context['angleZ'] = state.angleZ
		
# Main.	
class Window(Renderer, pyglet.window.Window):
    def __init__(self, drawing_cmds, **kw):
        pyglet.window.Window.__init__(self, **kw)
        self.renderer = GL_Renderer(drawing_cmds)

    def on_draw(self):
        self.clear()
        self.renderer.render()


class GL_Renderer(Renderer):
    def __init__(self, drawing_cmds):
        Renderer.__init__(self, drawing_cmds)
        self.meshs = []

    def post_draw(self, context):
        self.meshs = context['meshs'] 

    def get_context(self):
        return {'meshs': [], 'position': Vector(0, 0, 0), 'angleX': 0, 'angleY': 0, 'angleZ': 0}

    def render(self):
		print 'render'
		write_file(self.meshs, Path('output.obj'))
        #gl.glColor4f(1.0,0,0,1.0)
        #gl.glEnable (gl.GL_LINE_SMOOTH);
        #gl.glHint (gl.GL_LINE_SMOOTH_HINT, gl.GL_DONT_CARE)
        #gl.glLineWidth (2)
        #pts = []
        #for (x,y) in self.points:
        #    pts.append(x)
        #    pts.append(y)
        #print len(pts)
        #pyglet.graphics.draw(len(self.points), pyglet.gl.GL_LINE_STRIP, ('v2f', tuple(pts)))

def test(distanceTronc = 5, largeurTronc = 2, n = 3, x = 0, y = 0):
	turtle.delay(0)
	turtle.reset()
	turtle.up()
	turtle.goto(x, y)
	turtle.left(90)
	turtle.down()
	start_cmd = expand(distanceTronc, largeurTronc)
	#==
	rulemgr = RuleMgr( [ SplitRule() ])
	cmds =  rulemgr.apply( start_cmd, n)
	print len(cmds)
	#==
	drawing_cmds = [ DrawRotate(),
                     DrawForward(),
					 DrawPush(),
					 DrawPop(),
					 DrawRotate()]
	window = Window(drawing_cmds)
	window.renderer.draw(cmds)
	pyglet.app.run()


if __name__ == "__main__":
    test()